
// Playing Cards
// Justin Rankin
// Jesse Dorin

#include <iostream>
#include <conio.h>

using namespace std;



enum class Suit
{
	Hearts,
	Diamonds,
	Spades,
	Clubs
};
enum class Rank 
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

struct Card
{
	
	Rank Rank;
	Suit Suit;
};

void PrintCard(Card card) {
	string suit;
	string rank;
	Rank r1 = card.Rank;
	Suit s1 = card.Suit;

	// populate strings based on enum values
	if (s1 == Suit::Hearts) suit = "Hearts";
	else if (s1 == Suit::Spades) suit = "Spades";
	else if (s1 == Suit::Clubs) suit = "Clubs";
	else if (s1 == Suit::Diamonds) suit = "Diamonds";

	if (r1 == Rank::Two) rank = "Two";
	else if (r1 == Rank::Three) rank = "Three";
	else if (r1 == Rank::Four) rank = "Four";
	else if (r1 == Rank::Five) rank = "Five";
	else if (r1 == Rank::Seven) rank = "Six";
	else if (r1 == Rank::Eight) rank = "Eight";
	else if (r1 == Rank::Nine) rank = "Nine";
	else if (r1 == Rank::Ten) rank = "Ten";
	else if (r1 == Rank::Jack) rank = "Jack";
	else if (r1 == Rank::Queen) rank = "Queen";
	else if (r1 == Rank::King) rank = "King";
	else if (r1 == Rank::Ace) rank = "Ace";

	// print the submitted card	
	if (suit != "" && rank != "") cout << rank << " of " << suit << ".";

}

Card HighCard(Card card1, Card card2) {
	int value1;
	int value2;
	Suit suit1 = card1.Suit;
	Suit suit2 = card2.Suit;
	Rank rank1 = card1.Rank;
	Rank rank2 = card2.Rank;
	// compare card ranks
	if (rank1 > rank2) {
		return card1;
	}
	else if (rank2 > rank1) {
		return card2;
	}
	else if (rank1 == rank2) {

		// didn't want to change the order in the enumeration
		// convert Suit to correctly ordered values for card 1
		if (suit2 == Suit::Spades) value2 = 4;
		else if (suit2 == Suit::Diamonds) value2 = 3;
		else if (suit2 == Suit::Clubs) value2 = 2;
		else if (suit2 == Suit::Hearts) value2 = 1;
		// convert Suit to correctly ordered values for card 2
		if (suit1 == Suit::Spades) value1 = 4;
		else if (suit1 == Suit::Diamonds) value1 = 3;
		else if (suit1 == Suit::Clubs) value1 = 2;
		else if (suit1 == Suit::Hearts) value1 = 1;

		// compare the suit of both cards to break ties
		if (value1 > value2) {
			return card1;
		}
		else if (value2 > value1) {
			return card2;
		}
	}
	else return card1;

}

int main()
{
	Card c1;
	c1.Rank = Rank::Ace;
	c1.Suit = Suit::Spades;
	Card c2;
	c2.Rank = Rank::Ace;
	c2.Suit = Suit::Hearts;
	Card c3;
	c3.Rank = Rank::King;
	c3.Suit = Suit::Hearts;

	PrintCard(c3); 
	cout << "\n";
	PrintCard(c2); 
	cout << "\n";
	cout << "The high card is ";
	PrintCard(HighCard(c3, c2));

	(void)_getch();
	return 0;
}

